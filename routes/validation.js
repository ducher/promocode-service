const express = require('express');
const router = express.Router();
const controller = require('../controllers/validation');

router.post('/', function(req, res, next) { 
    return controller.validatePromo(req, res);
});

module.exports = router;