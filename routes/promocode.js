const express = require('express');
const router = express.Router();
const controller = require('../controllers/promocode');

/* GET a specific promocode*/
router.get('/:id', function(req, res, next) {
  res.send('respond with a resource');
});

/* POST a new promocode*/
router.post('/', function(req, res, next) {
    controller.createPromocode(req.body);
    res.send('ingest the resource');
});

module.exports = router;
