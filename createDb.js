const sqlite3 = require('sqlite3');
const config = require('./config');

const db = new sqlite3.Database(config.dbFileName);

db.serialize(() => {
    db.run("CREATE TABLE promocode (id TEXT PRIMARY KEY, name TEXT, percentage INTEGER, restrictions TEXT)");
});
   
db.close();
