const rp = require('request-promise-native');
const model = require('../models/promocodeRule');
const config = require('../config');

module.exports = {
    validatePromo,
}

function validatePromo(req, res) {
    const promo = {
        promocode_name: req.body.promocode_name,
        arguments: req.body.arguments,
    }
    return model.getPromocodeByName(promo.promocode_name)
        .then((row) => {
            console.log(row);
            validateAgainstRules(promo, row.restrictions).then( results => {
                let validationResult = {valid: true};
                for(let result of results){
                    if(!result.valid) {
                        validationResult = result;
                        break;
                    }
                }
                const responseJSON = {
                    promocode_name: row.name,
                    status: validationResult.valid ? 'accepted' : 'denied',   
                };
                if(validationResult.valid) {
                    responseJSON.avantage = { percent: row.percentage };
                } else {
                    responseJSON.reason = validationResult.reason;
                }
                res.send(responseJSON);
            });
            
        });
}

function validateAgainstRules(promo, rule) {
    let promises = [];
    const promoArgs = promo.arguments;
    for (var rKey in promoArgs) {
        if( promoArgs.hasOwnProperty(rKey) ) {
            if(rKey === 'meteo') {
                promises.push(validateMeteo(promoArgs[rKey].town, rule['@meteo']));
            } else if(rKey === 'age') {
                promises.push(validateOrRules('age', promoArgs[rKey], rule['@or']));
            } else if(rKey === 'date') {
                promises.push(validateDate(rule['@date']));
            }
        }
    }
    return Promise.all(promises);
}

// Or Rules only support the age validation for now
function validateOrRules(rName, rArg, orRules) {
    for (var i = 0; i < orRules.length; i++) {
        //the 'or' rules contain @age for instance
        if(orRules[i][`@${rName}`]){
            const eq = orRules[i][`@${rName}`].eq;
            if(eq){
                if(rArg === eq){
                    return {valid: true};
                }
            } else {
                const {lt, gt} = orRules[i][`@${rName}`];
                if(rArg >= gt && rArg <= lt) {
                    return {valid: true};
                }
            }
        }
    }
    return {valid: false, reason: `${rName}NotValid`};
}

function validateMeteo(location, rule) {
    return getWeatherForLocation(location)
    .then( weather => {
        const {gt, lt} = rule.temp;
        if(weather.temp < gt || weather.temp > lt) {
            return {valid: false, reason: {meteo: 'temperature'}};
        }
        if(weather.sky !== rule.is.toLowerCase()){
            return {valid: false, reason: {meteo: `isNot${rule.is}`}};
        }
        return {valid: true};
    });
}

function getWeatherForLocation(location) {
    // Uncomment for mock weather
    //return {temp: 15, sky: 'clear'};
    const options = {
        uri: `http://api.openweathermap.org/data/2.5/weather?q=${location}&units=metric&APPID=${config.weatherAPI}`,
        json: true,
    };

    return rp(options).then( (json) => {

        const weather = {
            sky: json.weather[0].main.toLowerCase(),
            temp: json.main.temp,
        }
        return weather;
    });
}

function validateDate(rule) {
    const now = Date.now();
    const after = new Date(rule.after);
    const before = new Date(rule.before);

    return {valid: (now >= after && now <= before)};
}