const model = require('../models/promocodeRule');

module.exports = {
    createPromocode,
}

function createPromocode(promocode) {
    const rule = {
        id: promocode._id,
        name: promocode.name,
        percentage: promocode.avantage.percent,
        restrictions: promocode.restrictions,
    }

    model.insertPromocodeRule(rule);
}