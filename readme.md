To run the project:

`npm install`
`node createDb.js`
Fill the Weather API key in config.js
`npm start`

To create the validation rule
```
POST http://localhost:3000/promocode
{
  "_id": "A1B3",
  "name": "WeatherCode",
  "avantage": { "percent": 20 },
  "restrictions": {
    "@or": [{
      "@age": {
	  "eq": 40
}
    }, {
      "@age": {
        "lt": 30,
        "gt": 15
      }
    }],
    "@date": {
      "after": "2017-05-02",
	"before": "2018-05-02"
    },
    "@meteo": {
      "is": "clear",
      "temp": {
        "gt": 15
      }
    }
  }
}
```

To validate a code
```
POST http://localhost:3000/validation
{
  "promocode_name": "WeatherCode",
  "arguments": {
    "age": 25,
    "meteo": { "town": "Miami" }
  }
}
```