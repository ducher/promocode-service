const sqlite3 = require('sqlite3');
const config = require('../config');

module.exports = {
    insertPromocodeRule,
    getPromocodeByName,
}

function insertPromocodeRule(rule){
    const db = new sqlite3.Database(config.dbFileName);
    db.serialize(() => {
        const restrictions = JSON.stringify(rule.restrictions);
        db.run(`INSERT OR REPLACE INTO promocode (id, name, percentage, restrictions)
        VALUES (?, ?, ?, ?)`, [rule.id, rule.name, rule.percentage, restrictions], (err) => {
            if (err) {
                return console.log(err.message);
            }
            console.log(`A row has been inserted with rowid ${this.lastID}`);
        });
    });
    db.close();
}

function getPromocodeByName(name) {
    const db = new sqlite3.Database(config.dbFileName);
    let sql = `SELECT * FROM promocode WHERE name=?;`;
    return new Promise(function (resolve, reject) {
        db.get(sql, [name], (err, row) => {
        if (err) {
            reject(err);
        }
        console.log(row.name);
        row.restrictions = JSON.parse(row.restrictions);
        resolve(row);
        });
    });
    db.close();
}